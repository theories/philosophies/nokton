# Contextual nokton automaton

Given :

- A nokton automaton $`(A,S,P,s^*,p^*,λ_{out},λ_{in},π)`$.
- A non empty countable subset $`A^* ⊂ A`$.
- A non empty countable subset $`S^* ⊂ S`$.
- A non empty countable subset $`P^* ⊂ P`$.

A nokton automaton $`(A^*,S^*,P^*,s^*,p^*,λ_{out},λ_{in},π)`$ is contextual case of the nokton automaton $`(A,S,P,s^*,p^*,λ_{out},λ_{in},π)`$ if :

- $`s^* ∈ S^*`$.
- $`p^* ∈ P^*`$.
- $`∀ \ (a,s) ∈ A^*×S^*, λ_{out}(a,s) ∈ S^*`$.
- $`∀ \ (t,s,p) ∈ \mathbb{N^*}×S^*×P^*, λ_{in}(t,s,p) ∈ P^*`$.
- $`∀ \ (p,a) ∈ P^*×(A\setminus A^*), π(p,a)=0`$.

***
[Nokton philosophy](../README.md)
