# theories/philosophies/nokton

## About this project

This project gives some ideas about the nature and how it works. It wants to be as general as possible. [Nokton project](https://gitlab.com/theories/physics/candidates/nokton) is a concrete example of this philosophy.

## Introduction

If we observe the nature, we notice that there is some laws fixing how it works. The mainstream physics theories are the proofs that theses laws exists. What's the quality of theses laws ? Can mainstream physics theories respond to all questions ?
They respond to many questions but not to all. Are some chance they give responses to all questions ? If there is something wrong from the start ?

In nokton philosophy, we assume that the nature is a nokton automaton.

## Nokton automaton

A nokton automaton is a 8-tuple $`(A,S,P,s^*,p^*,λ_{out},λ_{in},π)`$ where :

- $`A`$ a non empty finite set. We note $`\lvert{A}\rvert`$ the cardinality of this set.
- $`S`$ a non empty countable set.
- $`P`$ a non empty countable set.
- $`s^* ∈ S`$.
- $`p^* ∈ P`$.
- $`λ_{out}:A×S↦S`$.
- $`λ_{in}:\mathbb{N^*}×S×P↦P`$.
- $`π:P×A↦\mathbb{Q}_+`$ such as $`∀p ∈ P, \sum_{a ∈ A} π(p,a)=1`$.

We give the name :

- Set of actions for $`A`$.
- Set of external states for $`S`$.
- Set of internal states for $`P`$.
- Initial external state for $`s^*`$.
- Initial external state for $`p^*`$.
- External transitions function for $`λ_{out}`$.
- Internal transitions function for $`λ_{in}`$.
- Actions distribution for $`π`$.

### How a nokton automaton works

To understand how a nokton automaton works, we need to understand the nokton automaton cycle. Any nokton automaton cycle has as start :

- A external state $`s ∈ S`$.
- A internal state $`p ∈ P`$.

Here a description of the nokton automaton cycle steps :

- Using $`p`$ and the distribution actions $`π`$, the nokton automaton give us an action $`a`$ from actions set $`A`$. We name this operation a realization. The choice between possible realizations (from actions set $`A`$) is a random operation.
- Using the action $`a`$, $`s`$ and the external transitions function $`λ_{out}`$, the nokton automaton update its external state. The new external state is used as external state for the next nokton automaton cycle.
- Using the action $`a`$, $`p`$ and the internal transitions function $`λ_{in}`$, the nokton automaton update its internal state. The new internal state is used as internal state for the next nokton automaton cycle. The behavior of $`λ_{in}`$ can be different for each cycle.

So any nokton automaton cycle has 3 steps.
For the first nokton automaton cycle, we use $`s^*`$ and $`p^*`$.

## Natural nokton automaton

A natural nokton automaton is a nokton automaton where for given $`(t,t') ∈ \mathbb{N^*}×\mathbb{N^*}`$ such as $`t \mathrlap{\,/}{=} t'`$, $`∀`$ $`(s,p) ∈ S×P`$ $`λ_{in}(t,s,p)=λ_{in}(t',s,p)`$.
For given $`t_0 ∈ \mathbb{N^*}`$, $`λ^*_{in}:S×P↦P`$ is the natural internal transitions function defined as $`∀`$ $`(s,p) ∈ S×P`$ $`λ^*_{in}(s,p)=λ_{in}(t_0,s,p)`$.

## Contextual nokton automaton

Contextual nokton automaton is defined in this [document](content/contextual-nokton.md).

## Exploitation

Exploitation is defined in this [document](content/exploitation.md).

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) file for more details.

## Licenses

Any project content in this project is under [**Creative Commons LICENSE**](LICENSE.txt) and any project code content in this project is under [**MIT LICENSE**](LICENSE-CODE.txt).
